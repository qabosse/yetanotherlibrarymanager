module ReadersHelper
  def reader_link the_reader
   html = "<a href='/readers/#{the_reader.id}'>".html_safe
   html += the_reader.name
   html += "</a>".html_safe
   html += "(#{the_reader.mail})"
   html
  end
end
