module UsersHelper
  def user_link the_user
   html = "<a href='/users/#{the_user.id}'>".html_safe
   html += the_user.name
   html += "</a>".html_safe
   html
  end
end
