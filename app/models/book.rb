class Book < ApplicationRecord
  belongs_to :user
  belongs_to :reader, optional: true
  validates :title, :author, presence: { message: "Il faut un titre ET le nom de l'auteurice" }

  scope :by_user, lambda { |user|  where(user_id: user&.id)  }
  scope :by_reader, lambda { |reader|  where(reader_id: reader&.id)  }


  def lend_to(reader)
    success=true
    if reader
      if reader.user == self.user
        self.reader=reader
        self.loan_date=Date.today
      else
        success=false
      end
    else
      self.reader=nil
      self.loan_date=nil
    end
    self.save
    success
  end

end
