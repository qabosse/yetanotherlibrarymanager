class User < ApplicationRecord
  has_many :books
  has_many :users
  has_secure_password
  validates :name, :password, presence: { message: "Champ obligatoire" }
  validates :name, uniqueness: {message: "Ce nom est déjà pris"} 

  def admin?
    self.role=="admin"
  end
end
