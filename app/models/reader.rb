class Reader < ApplicationRecord
  belongs_to :user
  has_many :books
  validates :name, :mail, presence: { message: "Champ obligatoire" }
  validate :no_double_mail_among_user_readers
  validates :mail, format: { with: /\A.{1,}@.{1,}[.].*\z/,
   message: "Cette adresse mail est manifestement erronée" } #RFC 5322 is painstakingly understandable as a regexp
  scope :by_user, lambda { |user|  where(:user_id => user.id) }

  private

  def no_double_mail_among_user_readers
    if Reader.by_user(self.user)&.where.not(id: self.id).find_by(mail: self.mail)
      errors.add(:mail, "Vous avez déjà un·e lecteurice avec ce mail")
    end
  end

end
