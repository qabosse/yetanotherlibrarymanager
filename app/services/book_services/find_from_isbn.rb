module BookServices
  class FindFromIsbn
    attr_reader :book_details
    def initialize(isbn)
      #isbn doit être une chaine + virer les espaces inutiles
      @book_details=searchOpenLibraryApi(isbn)
    end

    def searchOpenLibraryApi(isbn)
      request = Typhoeus::Request.new(
        "https://openlibrary.org/api/books?bibkeys=ISBN:#{isbn}&jscmd=details&format=json",
        method: :get
        )
      request.run
      answer=JSON.parse(request.response.body)
      begin
        authors_data = answer["ISBN:#{isbn}"]["details"]["authors"]
        authors_list=[]
        authors_data.each { |author| authors_list.append(author["name"]) }
        title = answer["ISBN:#{isbn}"]["details"]["title"]
      rescue
        return nil
      else
        return { title: title, author: authors_list.join(' ,') }
      end
    end

  end
end
