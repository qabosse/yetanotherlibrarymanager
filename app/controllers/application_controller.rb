class ApplicationController < ActionController::Base
  before_action :set_current_user, :set_current_readers, :set_locale

  private

  def set_current_user
    if session[:user_id]
      begin
        @current_user = User.find(session[:user_id])
      rescue
        session[:user_id]=nil
      end
    end
  end

  def set_current_readers
    if @current_user
      @readers=Reader.by_user(@current_user)
    end
  end

  def set_locale
    if params[:locale]
      session[:locale]=params[:locale]
    end
    if session[:locale]
      I18n.locale = session[:locale]
    else
      I18n.locale = :en
    end
  end

end
