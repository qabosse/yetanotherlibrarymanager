class BooksController < ApplicationController
  def index
    @books=Book.by_user(@current_user)
    @book=Book.new
  end

  def show
    @book=Book.find(params[:id])
  end

  def lend
    @book=Book.find(params[:book_id])
    if params[:reader_id] !=''
      @reader=Reader.find(params[:reader_id])
    else
      @reader=nil
    end
    if @book.user == @current_user && @book.lend_to(@reader)
      flash[:info]="Vous avez prêté/rendu le livre"
    else
      flash[:info]="Vous n'avez pas le droit d'effectuer cette opération"
    end
    render "/books/show"
  end

  def complete_from_isbn
    book_details=BookServices::FindFromIsbn.new(params[:isbn]).book_details
    @book=Book.new(book_details)
    if !book_details
      @book.errors.add(:isbn, :invalid, message: "Aucune info associée à l'ISBN #{params[:isbn]}")
    end
    @book.user=@current_user
    @books=Book.all
    @isbn = params[:isbn]
    render "show"
  end

  def create
    if @current_user
      @book=Book.new(
        title: params[:title],
        author: params[:author],
        user: @current_user
      )
      if @book.save
        flash[:info]= "Livre ajouté !"
        redirect_to "/books"
      else
        render 'show'
      end
    else
      flash[:info] ="Vous devez être connecté pour ajouter un livre !"
      redirect_to "/books"
    end
  end

  def update
    @book=Book.find(params[:id])
    if @current_user == @book.user
      @book.update(
        title: params[:title],
        author: params[:author]
      )
      if @book.save
        flash[:info]= "Livre mis à jour !"
        redirect_to "/books/#{params[:id]}"
      else
        render 'show'
      end
    else
      flash[:info] ="Vous ne pouvez modifier que vos propres livres !"
      redirect_to "/books/#{params[:id]}"
    end
  end

  def destroy
    @book=Book.find(params[:id])
    if @current_user == @book.user
      @book.destroy
      flash[:info]= "Livre supprimé !"
    else
      flash[:info] ="Vous ne pouvez supprimer que vos propres livres !"
    end
    redirect_to "/books"
  end
end
