class ReadersController < ApplicationController
  def index
    @reader=Reader.new
  end

  def show
    @reader=Reader.find(params[:id])
    @books=Book.where(reader_id: @reader.id)
  end

  def create
    if @current_user
      @reader=Reader.new(
        name: params[:name],
        mail: params[:mail],
        user: @current_user
      )
      if @reader.save
        flash[:info]= "Lecteurice ajouté·e !"
        redirect_to "/readers"
      else
        render 'show'
      end
    else
      flash[:info] ="Vous devez être connecté pour ajouter des lecteurices !"
      redirect_to "/readers"
    end
  end

  def update
    @reader=Reader.find(params[:id])
    if @current_user == @reader.user
      @reader.update(
        name: params[:name],
        mail: params[:mail]
      )
      if @reader.save
        flash[:info]= "Infos modifiées !"
        render 'show'
      else
        render 'show'
      end
    else
      flash[:info] ="Vous ne pouvez modifier que vos propres lecteurices !"
      redirect_to "/readers/#{params[:id]}"
    end
  end

  def destroy
    @reader=Reader.find(params[:id])
    if @current_user == @reader.user
      @reader.destroy
      flash[:info]= "Lecteurice supprimé·e de la base de données !"
    else
      flash[:info] ="Vous ne pouvez supprimer que les lecteurices que vous avez vous-même ajouté·e·s !"
    end
    redirect_to "/readers"
  end
end
