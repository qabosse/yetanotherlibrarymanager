class SessionsController < ApplicationController
  def login
    @current_user = User.find_by(name: params[:name])&.authenticate(params[:password])
    if @current_user
      session[:user_id] = @current_user.id
      flash[:info] = "Vous êtes maintenant connecté"
      redirect_to "/books"
    else
      session[:user_id] = nil
      flash[:info] = "Échec de la connexion"
      redirect_to "/books"
    end
  end

  def logout
    session[:user_id] = nil
    flash[:info] = "Vous avez été déconnecté·e"
    redirect_to "/books"
  end
end
