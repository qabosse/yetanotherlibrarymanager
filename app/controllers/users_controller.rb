class UsersController < ApplicationController
  def index
    if @current_user&.admin?
      @users = User.all()
      @user = User.new()
    else
      @users= User.where(id:@current_user&.id)
    end
  end

  def create
    @users=User.all()
    if @current_user.admin?
      @user=User.new(
        name: params[:name],
        password: params[:password],
        )
      if @user.save
        flash[:info]= "Informations de l'utilisateurice enregistrées !"
        redirect_to "/users"
      else
        flash[:info]= "Une erreur s'est produite"
        render 'index'
      end
    else
      flash[:info] ="Vous devez avoir des droits d'administration pour ajouter des utilisateurices !"
      redirect_to "/users"
    end
  end

  def update
    @user=User.find(params[:id])
    if @current_user == @user || @current_user&.admin?
      @user.update(
        name: params[:name],
        password: params[:password]
      )
      if @user.save
        flash[:info]= "Infos mises à jour !"
        redirect_to "/users/#{params[:id]}"
      else
        render 'show'
      end
    else
      flash[:info] ="Vous n'avez pas de droits suffisants pour effectuer cette opération !"
      render 'show'
    end
  end

  def destroy
    @user=User.find(params[:id])
    if @current_user == @user || @current_user&.admin?
      Book.by_user(@user).destroy_all
      Reader.by_user(@user).destroy_all
      @user.destroy
      flash[:info]= "Compte utilisateurice supprimé, ainsi que toutes les informations associées"
    else
      flash[:info] ="Vous n'avez pas les droits pour effectuer cette opération !"
    end
    redirect_to "/users"
  end

  def show
    @user=User.find(params[:id])
    @books=Book.by_user(@user)
    @readers=Reader.by_user(@user)
  end

end
