Rails.application.routes.draw do
  scope "(:locale)", locale: /en|fr/ do
    root 'static_pages#home'
    get 'static_pages/home'
    get 'static_pages/about'
    get 'static_pages/help'
    post 'sessions/login'
    get 'sessions/logout'
    resources :readers, only: [:index, :show, :create, :update, :destroy]
    resources :books, only: [:index, :show, :create, :update, :destroy]
    resources :users, only: [:index, :show, :create, :update, :destroy]
    post 'books/:book_id' => "books#lend"
    get 'complete_from_isbn' => "books#complete_from_isbn"
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
