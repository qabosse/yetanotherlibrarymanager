# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


admin = User.create(name: 'admin', password:"admin", role: 'admin')
cecile = User.create(name: 'Cécile', password:"toto")
lorraine = User.create(name:"Lorraine", password:"bisou")

Book.create(title: 'Simple', author: "Ottolenghi", user: lorraine)
Book.create(
  title: 'Design Patterns catalogue de modèles de conception réutilisables',
  author: "Erich Gamma, Richard Helm, Ralph Johnson et John Vlisssides",
  user: cecile
)

Reader.create(name:"Esther", mail:"esther@foo.tld", user: cecile)
Reader.create(name:"Tristan", mail:"tristan@foo.tld", user: cecile)
