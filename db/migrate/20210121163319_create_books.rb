class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :title
      t.string :author
      t.integer :isbn
      t.date :loan_date
      t.references :user, foreign_key: true
      t.references :reader, foreign_key: true

      t.timestamps
    end
  end
end
